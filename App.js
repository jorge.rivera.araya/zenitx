/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import Zenitx from './src'

const App = () => <Zenitx/>

export default App;
